The folder filib-3.0.2 contains FILIB++ v3.0.2 as downloaded on January 22, 2019 from www2.math.uni-wuppertal.de/~xsc/software/filib.html  
For license information, please refer to the files therein.

The only changes to the source code are the following:
	- removed dynamic exception specifications to ensure compatibility with C++17
