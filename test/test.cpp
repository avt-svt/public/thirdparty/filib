 /**********************************************************************************
 * Copyright (c) 2019 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This file is made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/

#include <iostream>

#include "interval/interval.hpp"


int main() {

	filib::interval<double> x(-1,1);
	filib::interval<double> f = x*x+2.*x;
	
	std::cout << std::endl << "f := x*x + 2*x" << std::endl << std::endl;
	std::cout << std::endl << "f(" << x << ") = " << f << std::endl << std::endl;
	std::cout << std::endl << "FILIB++ seems to work..." << std::endl;
	std::cout << "Note that this test is merely intended to test compilation / setup of the repository and CmakeLists.txt" << std::endl;
	std::cout << "Further tests can be found in filib-3.0.2/test" << std::endl;
	return 0;

}